import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';


/*
  Generated class for the CustomerSupport page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-privacy',
  templateUrl: 'privacy.html'
})
export class Privacy {
  
  
  constructor( public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad Privacy Page');
  }

   dismiss(){
  		this.viewCtrl.dismiss();
  }

  

}
