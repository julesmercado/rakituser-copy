import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Notifications} from '../notifications/notifications';
import { RatingModule } from "ngx-rating";

import * as firebase from 'firebase/app'
/*
  Generated class for the NotificationsBids page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-job-details',
  templateUrl: 'job-details.html'
})
export class JobDetails{

	profileSegment: string = "Buzzwall";
  jobDetails: any = {};
  monthNames: any = [];
  review: string;
  starsCount: number = 0;

  @ViewChild('maps') mapElement;
  map: any;
  marker: any;
  markers: any = [];
  infowindow: any;
  geocoder: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private alertCtrl: AlertController) {
    
    this.jobDetails = navParams.get('jobDetails');
    this.monthNames = [ "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
                    "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" ];
    this.processData( this.jobDetails )
  }


	  dismiss(){
		    this.viewCtrl.dismiss();
	 	}
    ngOnInit(){
      var self = this;
      var lat = self.navParams.get('jobDetails');
      var latLng = lat.latLng.split("-")
      self.setMap( parseFloat(latLng[0]), parseFloat(latLng[1]) )
    }

    processData( data: any ){
      var self = this;
      var now = new Date( data.date + " " + data.time )
      var latLng = data.latLng.split("-")
      var thisDay = now.getDay();
      data.dateWanted = self.monthNames[now.getMonth()] + ' ' + now.getDate() + ', ' + now.getFullYear();
      var options = {
        hour: 'numeric',
        minute: 'numeric',
        hour12: true
      };
      console.log( thisDay )
      data.timeWanted = now.toLocaleString('en-US', options);
      if( data.hired ){
        self.getUserData( data.hired.id, data )
      }
      if( data.status == "Completed" ){
        //self.getReview( data.hired.id )
      }
    }

    getUserData( id: string, job: any ){
      var self = this;

      firebase.database().ref( "/users/" + id ).once('value').then( function( snap ){
          console.log( snap.val() )
          job.hired.profile = snap.val();
      } )
    }
    setMap( lat: number, lang: number ){
      var self = this;
      console.log( lat, lang )
      let latLng = new google.maps.LatLng(lat, lang);
      let mapOptions = {
        center: latLng,
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      self.geocoder = new google.maps.Geocoder;
      self.infowindow = new google.maps.InfoWindow;

      self.map = new google.maps.Map(self.mapElement.nativeElement, mapOptions)
      self.marker = new google.maps.Marker({
          position: latLng,
          map: this.map,
          draggable: false
        });

    }

    geocodePosition(pos) {
      var self = this;
      self.geocoder.geocode({
        latLng: pos
      }, function(responses) {
        if (responses && responses.length > 0) {
          self.marker.formatted_address = responses[0].formatted_address;
        } else {
          self.marker.formatted_address = 'Cannot determine address at this location.';
        }
        self.infowindow.setContent(self.marker.formatted_address + "<br>coordinates: " + self.marker.getPosition().toUrlValue(6));
        self.infowindow.open(self.map, self.marker);
      });
    }

    sendReview( review: string, id: string ){
      var self = this;
      console.log( review, id )
      if( self.starsCount != 0 ){
          firebase.auth().onAuthStateChanged(function(userAf) {
            firebase.database().ref(`/reviews/` + id + "/asArtist/" + userAf.uid)
                    .set({"star": self.starsCount, "review": review, "name": userAf.displayName, "photoURL": userAf.photoURL})
                    .then( function( res ){
                      self.presentAlert( "Great", "Review posted!")
                    } )
          } )
      }else{
        self.presentAlert( "Wait", "You forgot the stars")
      }
      
    }

    getReview( id: string ){
      var self = this;
      firebase.auth().onAuthStateChanged(function(userAf) {
        console.log( userAf.uid, id )
        firebase.database().ref(`/reviews/` + id + "/asArtist/" + userAf.uid).once( 'value' )
                .then( function( snap ){
                  console.log( snap.val() )
                  if( snap.val() ){
                    self.starsCount = snap.val().star || 0;
                    self.review = snap.val().review
                  }
                  
                } )
      } )
    }
    presentAlert( title: string, sub: string ) {
      var self = this;
      let alert = this.alertCtrl.create({
        title: title,
        subTitle: sub,
        buttons: [{
            text: 'OK',
            handler: data =>{
              if( self.starsCount != 0 ){
                self.closePage()
              }
              
            }
        }]
      });
      alert.present();
    }
    closePage(){
        this.viewCtrl.dismiss();
    }
}
