import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, LoadingController, ViewController } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UserService } from '../../providers/user-service';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { JobDetails } from '../job-details/job-details';
/*
  Generated class for the MyJobOffer page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-my-job-offer',
  templateUrl: 'my-job-offer.html',
  providers: [UserService]
})
export class MyJobOffer{
  private url = 'http://rakit-test-one.herokuapp.com';//'https://rakit-test-one.herokuapp.com';
  open: any = [];
  inProgress: any = [];
  cancelled: any = [];
  completed: any = [];
  closed: any = [];
  userDetail: any = {};
  toUpdateUserJob: FirebaseObjectObservable<any>;
  toUpdateUserJobAll: FirebaseObjectObservable<any>;
  hiredInJobs: FirebaseObjectObservable<any>;
  items :  FirebaseListObservable<any>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http   : Http,
    private loadingCtrl: LoadingController, 
    public viewCtrl: ViewController,
    private alertCtrl: AlertController,
    private af: AngularFireDatabase,
    private modalCtrl: ModalController) {

      

    }

  dismiss(){
  		this.viewCtrl.dismiss();
  }


	ionViewDidLoad() {
      var self = this;
	    console.log('ionViewDidLoad MyJobOfferPage');
      self.getUser()
     
	}

  getUser(){
    var self = this;
    firebase.auth().onAuthStateChanged(function(userAf) {
        if (userAf) {
          self.userDetail = {
            name: userAf.displayName,
            email: userAf.email,
            photo: userAf.photoURL,
            uid: userAf.uid
          }
          self.items = self.af.list('/jobsByUser/' + userAf.uid);
          self.items.subscribe(
                val => self.dependingOnStatus( val ), 
          )
        } else {
          // No user is signed in.
          console.log( "No User" )
        } 
      });
  }
  viewJob( job: object ){
    this.presentLoading();
        let openBids = this.modalCtrl.create(JobDetails, {jobDetails: job});

        setTimeout(() => {
          openBids.present();
        }, 500);

  }

  cancelJob( key: any, subCat: string, id: number ){
    var self = this;
    self.cancelAlert( key, subCat, id )
  }
  completeJob( key: any, hiredId: string, subCat: string, id: number ){
    var self = this;
    self.completeAlert( key, hiredId, subCat, id )
  }

  dependingOnStatus( array: any ){
    var self = this;
    console.log( array )
    self.open = [];
    self.inProgress = [];
    self.cancelled = [];
    self.completed = [];
    self.closed = [];

      array.forEach( function( e ){
            if( e.status == 'Open' ){
                  e.key = e.$key;
                  e.low = e.budget.slice( 0, e.budget.indexOf('-') );
                  e.high = e.budget.slice( e.budget.indexOf('-')+1, e.budget.length )
                  self.open.push( e );
            }else if( e.status == 'In Progress' ){
                  e.key = e.$key;
                  e.low = e.budget.slice( 0, e.budget.indexOf('-') );
                  e.high = e.budget.slice( e.budget.indexOf('-')+1, e.budget.length )
                  self.inProgress.push( e );
            }else if( e.status == 'Cancelled' ){
                  e.key = e.$key;
                  e.low = e.budget.slice( 0, e.budget.indexOf('-') );
                  e.high = e.budget.slice( e.budget.indexOf('-')+1, e.budget.length )
                  self.cancelled.push( e );
            }else if( e.status == 'Completed' ){
                  e.key = e.$key;
                  e.low = e.budget.slice( 0, e.budget.indexOf('-') );
                  e.high = e.budget.slice( e.budget.indexOf('-')+1, e.budget.length )
                  self.getUserData( e.hired.id, e )
                  self.completed.push( e );
            }
            else if( e.status == 'Closed' ){
                  e.key = e.$key;
                  e.low = e.budget.slice( 0, e.budget.indexOf('-') );
                  e.high = e.budget.slice( e.budget.indexOf('-')+1, e.budget.length )
                  self.closed.push( e );
            }
      } )
  }
  getUserData( id: string, job: any ){
    var self = this;

      firebase.database().ref( "/users/" + id ).once('value').then( function( snap ){
          console.log( snap.val() )
          job.hired.profile = snap.val();
      } )
  }
  dateFormat( date: string ){
    return date;
  }
  presentLoading() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Please wait...',
      showBackdrop: true,
      dismissOnPageChange: false,
      duration: 2000
                    });
    loading.present();
    console.log("spinner");
  }
  cancelAlert( key: string, subCat: string, id ) {
    var self = this;
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      subTitle: 'You will not be able to undo this',
      buttons: [{
          text: "Cancel",
          role: "cancel"
        },{
          text: "Ok",
          handler: data =>{
            self.toUpdateUserJob = self.af.object( '/jobsByUser/' + self.userDetail.uid + "/" + key );
            self.toUpdateUserJobAll = self.af.object( '/jobs/' + subCat + "/" + key );
            self.toUpdateUserJob.update( {"status": "Cancelled"} )
            self.toUpdateUserJobAll.update( {"status": "Cancelled"} )
            self.open.splice( id, 1 )
          }
      }]
    });
    alert.present();
  }
  completeAlert( key: string, hiredId: string, subCat: string, id ) {
    var self = this;
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      subTitle: 'You will not be able to undo this',
      buttons: [{
          text: "Cancel",
          role: "cancel"
        },{
          text: "Ok",
          handler: data =>{
            self.toUpdateUserJob = self.af.object( '/jobsByUser/' + self.userDetail.uid + "/" + key );
            self.toUpdateUserJobAll = self.af.object( '/jobs/' + subCat + "/" + key );
            self.hiredInJobs = self.af.object('/hiredInJobs/' + hiredId + "/" + key);
            self.toUpdateUserJob.update( {"status": "Completed"} )
            self.toUpdateUserJobAll.update( {"status": "Completed"} )
            self.hiredInJobs.update( {"status": "Completed"} )
            self.closed.splice( id, 1 )
          }
      }]
    });
    alert.present();
  }
}
