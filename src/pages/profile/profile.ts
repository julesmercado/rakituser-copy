import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, ViewController, ModalController, Events } from 'ionic-angular';
import { Login } from '../login/login';
import * as firebase from 'firebase/app';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';

/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [  ],
})
export class Profile{
  name: string;
  userInfo: any = {};
  userAf: any = {};
  items: FirebaseObjectObservable<any>;
  constructor( private viewCtrl: ViewController, private af: AngularFireDatabase,
                private alertCtrl: AlertController ) {

    this.userInfo = {
      name: "",
      gender: "",
      email: "",
      id: "",
      image: "",
      phone: ""
    };
    this.userAf = {
      name: "",
      gender: "",
      email: "",
      id: "",
      image: "",
      phone: ""
    };
  }

  ionViewDidLoad() {
      var self = this;
      self.getUser()
  }

  presentAlert( title: string, subTitle: string, button: string ) {
    var self = this;
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: [{
          text: button,
          handler: data =>{
            self.closePage()
          }
      }]
    });
    alert.present();
  }

  getUser(){
      var self = this;
      firebase.auth().onAuthStateChanged(function(userAf) {
        if (userAf) {
          self.userAf = {
            name: userAf.displayName,
            email: userAf.email || "",
            photoURL: userAf.photoURL,
            uid: userAf.uid
          };
          self.userInfo = {
            name: userAf.displayName,
            email: userAf.email || "",
            photoURL: userAf.photoURL,
            uid: userAf.uid
          }
          self.items = self.af.object('/users/' + userAf.uid);
          self.items.subscribe(
                val => {
                  console.log( val )
                  self.userInfo.phone = val.phone || "";
                  self.userInfo.affiliations = val.affiliations || "";
                }
          )
        } else {
          // No user is signed in.
          console.log( "No User" )
        }
      });
  }

  save(){
    var self = this;
    self.items.update( self.userInfo )
        .then( function( res ){
          if( self.userInfo.affiliations == '' || self.userInfo.phone == '' ){
            self.presentAlert( 'Oops', "You can\'t leave your profile incomplete", "Ok" )
          }else{
            self.presentAlert( 'Alright', "Let\'s find you some Artists", "Ok" )
          }
        } )
        .catch( function( err ){
          self.presentAlert( 'Server error', "Oops! Something went wrong", "Dismiss" )
        } )
  }

  logout(){
      var self = this;
      //console.log( this.nativeStorage );
      // self.nativeStorage.remove('user').then(function(response){
      //   self.fb.logout();
      //   self.viewCtrl.dismiss();
      //   window.location.reload(); 
      // }).catch( function( data ) {
      //         console.log( data )
      //     } )
      firebase.auth().signOut().then(function() {
        // Sign-out successful.
        window.location.reload();
      }, function(error) {
        // An error happened.
        console.log( error )
      }); 
  }

  closePage(){
  		this.viewCtrl.dismiss();
  }

}
