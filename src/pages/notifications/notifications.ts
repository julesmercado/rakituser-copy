import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { NotificationsBids } from './notifications-bids/notifications-bids';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UserService } from '../../providers/user-service';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app';

/*
  Generated class for the Notifications page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
  providers: [UserService]
})
export class Notifications{
  private url = 'http://rakit-test-one.herokuapp.com';//'https://rakit-test-one.herokuapp.com';
  open: any = [];
  inProgress: any = [];
  cancelled: any = [];
  completed: any = [];
  items :  FirebaseListObservable<any>;
  userDetail: any = {};
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private modalCtrl: ModalController,
              private loadingCtrl: LoadingController, 
              private af: AngularFireDatabase,
              public http   : Http ) {}

presentLoading() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Please wait...',
      showBackdrop: true,
      dismissOnPageChange: false,
      duration: 2000
                    });
    loading.present();
    console.log("spinner");
  }

  ionViewDidLoad() {
      var self = this;
      console.log('ionViewDidLoad MyJobOfferPage');
      self.getUser()
  }

  getNotification( id: string ){
    var self = this;
    var rootRef = firebase.database().ref();
    var userRef = rootRef.child("jobsByUser").child(id);
    var query   = userRef
                      .orderByChild("interestedLength")
                      .startAt(1);
    query.on('child_added', 
      snap => {
        console.log( snap.val() )
        self.dependingOnStatus( snap.val(), snap.key )
    })
    
  }

	previewInterested( details: object ){
			 this.presentLoading();
  		  let openBids = this.modalCtrl.create(NotificationsBids, {"job": details});

        setTimeout(() => {
          openBids.present();
        }, 500);

	}

  getUser(){
    var self = this;
    firebase.auth().onAuthStateChanged(function(userAf) {
        if (userAf) {
          self.userDetail = {
            name: userAf.displayName,
            email: userAf.email,
            photo: userAf.photoURL,
            uid: userAf.uid
          }
          self.getNotification(userAf.uid)
        } else {
          // No user is signed in.
          console.log( "No User" )
        }
      });
  }
  dependingOnStatus( job: any, keyString: string ){
    var self = this;
    console.log( job, keyString )
    job.keyString = keyString;
    if( job.status == 'Open' ){
          job.low = job.budget.slice( 0, job.budget.indexOf('-') );
          job.high = job.budget.slice( job.budget.indexOf('-')+1, job.budget.length )
          if( typeof(job.interested) === "object" ){
            var arrayInterested = [];
            for( var key in job.interested ){
              //provide id of object inside of value
              job.interested[key].id = key;
              //push the object ot a temporary array
              arrayInterested.push( job.interested[key] )
            }
            job.tempInterested = arrayInterested;
          }
          console.log( typeof(job.interested) )
          
          self.open.push( job );
    }else if( job.status == 'In Progress' ){
          self.inProgress.push( job );
    }else if( job.status == 'Cancelled' ){
          self.cancelled.push( job );
    }else if( job.status == 'Completed' ){
          self.completed.push( job );
    }
  }

  processDate( date: number ){
    return new Date( date )
  }
}
