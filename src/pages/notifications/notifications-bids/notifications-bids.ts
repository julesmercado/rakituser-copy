import { Component } from '@angular/core';
import { NavController, AlertController, ModalController, NavParams, ViewController, App } from 'ionic-angular';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import * as firebase from 'firebase/app';

import { FullImage } from '../../full-image/full-image'
import { Page1 } from '../../page1/page1';
//import { Notifications} from '../notifications/notifications';
/*
  Generated class for the NotificationsBids page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-notifications-bids',
  templateUrl: 'notifications-bids.html'
})
export class NotificationsBids{

  userVideoLink: SafeResourceUrl;

	profileSegment: string = "Wall";
  job: any = [];

  constructor(private navCtrl: NavController,
    private navParams: NavParams,
    private viewCtrl: ViewController,
    private af: AngularFireDatabase,
    private sanitizer: DomSanitizer,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private app: App) {
    this.job = navParams.get('job');
  }

  ionViewDidLoad(){
    this.processInterestedProfiles( this.job.tempInterested )
  }
  dismissBids(){
	  this.viewCtrl.dismiss();
 	}
  
  transform( link: string ){
    return this.sanitizer.bypassSecurityTrustResourceUrl(link)
  }

  processInterestedProfiles( interested: any ){
    var self = this;
    interested.forEach( function( e ){
      e.profileObject = new FirebaseObjectObservable();
      e.profileObject = self.af.object('/users/' + e.id);
      e.profileObject.subscribe(
              val => {
                e.profile = val;
                e.profile.jobsCompleted = val.jobsCompleted || 0;
                var videoLink = "";
                videoLink = e.profile.video || "";
                var link = [];
                if( videoLink ){
                  link = videoLink.match( /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/ )
                  videoLink = "https://www.youtube.com/embed/" + link[1];
                  e.profile.sanitizedVideo = self.transform(videoLink);
                }
                console.log( "hey there" )
                self.getReviews( e.profile.uid, e )
              }
      )

    } )
  }

  getReviews( id: string, profile: any ){
    profile.reviews = [];
    var rootRef = firebase.database().ref();
    var userRef = rootRef.child("reviews").child(id);
    var query   = userRef
                      .child("asArtist");
    query.on('child_added', 
      snap => {
        var obj = {
          "star": snap.val().star, 
          "review": snap.val().review, 
          "name": snap.val().name, 
          "photoURL": snap.val().photoURL,
          "key": snap.val().key
        }
        profile.reviews.push( obj )
    })
    userRef.once('value', function( snap ){
      if( snap.val() != null ){
        profile.meanStarCountArtist = snap.val().meanStarCountArtist || 0;
      }
    })
    
  }

  enlargeImage( link: string ){
    let openNavPage = this.modalCtrl.create(FullImage, {img: link});
          openNavPage.present();
  }


  hireArtist( name: string, id: string, photo: string, bid: string ){
    var self = this;
    var hire = {
      id: id,
      bid: bid,
      name: name,
      photoURL: photo
    }

    self.job.status = "Closed";
    
    
    firebase.auth().onAuthStateChanged(function(userAf) {
      var jobsByUserRef = firebase.database().ref(`/jobsByUser/` + userAf.uid + "/" + self.job.keyString + "/hired");
      var allJobsRef = firebase.database().ref(`/jobs/` + self.job.subcatId + "/" + self.job.keyString + "/hired");
      var hiredInJobs = firebase.database().ref(`/hiredInJobs/` + id + "/" + self.job.keyString);
      
      jobsByUserRef.set(hire)
              .then( function( res ){
                firebase.database().ref(`/jobsByUser/` + userAf.uid + "/" + self.job.keyString)
                        .update( {"status": "Closed"} )
              } )
      allJobsRef.set(hire)
              .then( function( res ){
                firebase.database().ref(`/jobs/` + self.job.subcatId + "/" + self.job.keyString)
                        .update( {"status": "Closed"} )
              } )
      self.job.interested = null;
      self.job.tempInterested = null;
      self.job.hired = hire;
      hiredInJobs.set( self.job )
      self.viewCtrl.dismiss().then(() => {
        self.app.getRootNav().setRoot(Page1);
      });
    });
  }
  callNumber( number: string ){
    let call = "tel:" + number;
    window.location.href = call;
  }

  presentAlert( name: string, id: string, photo: string, bid: string ) {
    var self = this;
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      subTitle: 'You want to hire ' + name,
      buttons: [{
          text: "Cancel",
          handler: data =>{
            
          }
      },{
          text: "Ok",
          handler: data =>{
            self.hireArtist( name, id, photo, bid )
          }
      }]
    });
    alert.present();
  }


}
