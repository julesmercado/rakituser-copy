import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import * as firebase from 'firebase/app';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';

import { Privacy } from '../privacy/privacy';
import { Terms } from '../terms/terms';


/*
  Generated class for the CustomerSupport page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-customer-support',
  templateUrl: 'customer-support.html'
})
export class CustomerSupport {
  items: FirebaseObjectObservable<any>;
  support: FirebaseListObservable<any>;
  userAf: any;
  userInfo: any;
  input: string;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController, 
    private alertCtrl: AlertController,
    private af: AngularFireDatabase) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomerSupportPage');
    this.getUser()
  }

   dismiss(){
  		this.viewCtrl.dismiss();

  }
  toPrivacy(num: number){
    if( num == 1 ) this.navCtrl.push(Privacy); 
    else this.navCtrl.push(Terms)
  }

  submitQuery( input: string ){
    var self = this;
    var dataToPass = {
      input: input,
      by: self.userInfo
    }
    console.log( dataToPass )
    self.postSupport( dataToPass )
  }

  getUser(){
    var self = this;
    firebase.auth().onAuthStateChanged(function(userAf) {
        if (userAf) {
          self.userAf = {
            name: userAf.displayName,
            email: userAf.email || "",
            photo: userAf.photoURL,
            uid: userAf.uid
          };
          self.userInfo = {
            name: userAf.displayName,
            email: userAf.email || "",
            photo: userAf.photoURL,
            uid: userAf.uid
          }
          self.support = self.af.list('/support/' + self.userAf.uid)
          self.items = self.af.object('/users/' + userAf.uid);
          self.items.subscribe(
                val => {
                  console.log( val )
                  self.userInfo.phone = val.phone || "";
                  self.userInfo.affiliations = val.affiliations || "";
                }
          )
        } else {
          // No user is signed in.
          console.log( "No User" )
        }
      });
  }

  postSupport( supportInput: object ){
    var self = this;
    self.support
        .push( supportInput )
        .then( data =>{
            let alert = self.alertCtrl.create({
              title: 'Rakit Team',
              subTitle: 'Thank you for contributing to make the app better. We will consider your inputs.',
              buttons: [{
                text : 'Ok',
                handler: data => {
                  self.dismiss();
                }
              }],
              
            });
            alert.present();
        } );

  }

}
