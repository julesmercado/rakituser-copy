import { Component } from '@angular/core';

import { NavController, NavParams,  ModalController, 
         AlertController, Events, App } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

import { Broadcaster } from '@ionic-native/broadcaster';
import { Http, Response, Headers, RequestOptions }    from '@angular/http';
import { Platform } from 'ionic-angular';

import { Page1 } from '../page1/page1';
import { Tips } from '../tips/tips';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { UserService } from '../../providers/user-service';

@Component({
  selector: 'page-login',  
  templateUrl: 'login.html',
  providers: [ UserService, Broadcaster ],
})
export class Login {
  email: any;  name: any;  id: any;  randomQuote: any; plat: any; fbProv: any; alertCtrl: any;//fb: any;
  service: any; options: any; headers: any;

  userInfo: any;
  url: any; 
  buttonName: string;
  constructor( public navCtrl: NavController,
  	           public navParams: NavParams, 
  	           public modalCtrl: ModalController, public http: Http,
               public platform: Platform, private fb: Facebook, 
               private ac: AlertController, public events: Events, 
               private broadcaster: Broadcaster,
               private afAuth: AngularFireAuth,
               private app: App
  	) {
    //
        this.headers = new Headers({ 'Content-Type': 'application/json' });
        this.options = new RequestOptions({ headers: this.headers });
        this.email = '';
        this.name = '';
        this.id = '';
        this.alertCtrl = ac;
        this.url = 'http://rakit-test-one.herokuapp.com';
        this.buttonName = "Login with Facebook ";
  }

  ngOnInit(){
    var self = this;
    //self.user = UserService;
    // if user is logged in to facebook
    self.letIn()
  }
  letIn(){
    var self = this;
    firebase.auth().onAuthStateChanged(function(userAf) {
      if (userAf) {
        
        self.userInfo = {
          name: userAf.displayName,
          email: userAf.email || "",
          image: userAf.photoURL,
          uid: userAf.uid
        }
        console.log( self.userInfo )
        self.events.publish('user:ready', self.userInfo);
        self.navCtrl.setRoot(Tips);  
      } else {
        // No user is signed in.
        console.log( "No User" )
      }
    });
  }
 

  
  
  login() {
        var self = this;
        self.buttonName = "Logging in..."
        if(self.platform.is('cordova')) {
          self.fb.login(['public_profile', 'user_friends', 'email'])
                  .then((res: FacebookLoginResponse) => {
                      const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
                      //self.events.publish('user:isLoggedInFb', res);
                      firebase
                        .auth()
                        .signInWithCredential(facebookCredential)
                        .then( response =>{
                          console.log( response )
                          var user = {
                            name: response.displayName,
                            email: response.email,
                            photoURL: response.photoURL
                          }
                          self.events.publish('user:ready', user);
                          self.navCtrl.setRoot(Tips); 

                        } ); 
                  })
                  .catch((e) => { self.buttonName = "Login with Facebook ", this.showAlerts( 'Error', 'Error logging into Facebook' ), console.log(e)})
                  //.reject(e =>{ self.showAlert( 'Error', 'Error logging into Facebook' ) });
        
        } else {
            console.log("Please run me on a device");
            self.afAuth.auth
            .signInWithPopup(new firebase.auth.FacebookAuthProvider())
            .then(res => {
              var details = {
                  name: res.additionalUserInfo.profile.name,
                  email: res.additionalUserInfo.profile.email, 
                  id: res.additionalUserInfo.profile.id,
                  fbId: res.additionalUserInfo.profile.id,
                  photoURL: res.additionalUserInfo.profile.picture.data.url,
                  gender: res.additionalUserInfo.profile.gender || ""
              }
              self.events.publish('user:ready', details);
              self.navCtrl.setRoot(Tips);  
            });
        }
  }

  changeRoute(){
    this.navCtrl.setRoot(Page1);
  }
  

  logError( err: any ){
    console.log( err )
  }
  



  showAlerts( title, message ) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

}
