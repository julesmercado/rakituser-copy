import { Component } from '@angular/core';

import { NavParams, ViewController, AlertController,  ModalController} from 'ionic-angular';
//import { CreateJobOffer} from '../../create-job-offer/create-job-offer'
//import { Page1 } from '../../page1/page1';
import { BookingSuccess} from './booking-success/booking-success';
import { UserService } from '../../../providers/user-service';
import { Observable} from 'rxjs/Rx';
import { AngularFire, AuthProviders, AuthMethods } from 'angularFire2';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app'

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
	selector: 'modal-bookingOfferModal',
    templateUrl: `booking-offer-modal.html`,
    providers: [ UserService ]

})
export class BookingOfferModal {
	jobsBySubCat: FirebaseObjectObservable<any>;
	jobsByUser	: FirebaseListObservable<any>;
	bookDetails: any={};
	dataToSend : any={};
	user 	   : UserService = {};
	userDetail : any = {};
	subcatName : string;
	items	   : FirebaseObjectObservable<any>;

    constructor(
    			public viewCtrl : ViewController, 
    			public params 	: NavParams, 
    			public modalCtrl: ModalController,
    			private alertCtrl: AlertController,
    			private af: AngularFireDatabase) {
    	this.bookDetails = params.get('bookDetails');
    	this.subcatName = params.get('cat')
    	this.userDetail = {

    	}
    }
    profile = {};

    //close modal
    dismiss() {
	 
	   this.viewCtrl.dismiss();
	 }

	ngOnInit(){
		//var self = this;
	}
	
	finalizeBooking( user: object ){
		var self = this;
		var date = firebase.database.ServerValue.TIMESTAMP;
		self.dataToSend = {
			"job":{
				//"specialization_id": "",
				"subcatId": self.bookDetails.booking_talent,
				"description": self.bookDetails.booking_description,
				"address": self.bookDetails.booking_location,
				"latLng": self.bookDetails.booking_latLng,
				//"date_time": self.bookDetails.booking_date + " " + self.bookDetails.booking_time,
				"date": self.bookDetails.booking_date,
				"time": self.bookDetails.booking_time,
				"budget": self.bookDetails.booking_budget_start + "-" + self.bookDetails.booking_budget_end,
				"status": "Open",
				"created_at": date,
				"by": user,
				"interested": "not set"
			}
		}
		console.log( self.dataToSend )
		self.postJob(self.dataToSend.job)
	}

	getUser(){
	    var self = this;
	    var user = UserService.User;
	    console.log( user )
	    
	    firebase.auth().onAuthStateChanged(function(userAf) {
	      if (userAf) {
	      	var items = firebase.database().ref('/users/' + userAf.uid);
	        items.once('value').then(
	            function ( snapshot ){
	              	self.userDetail = {
			        	name: userAf.displayName,
			        	email: userAf.email,
			        	photoURL: userAf.photoURL,
			        	uid: userAf.uid,
			        	phone: snapshot.val().phone || "",
			        	affiliations: snapshot.val().affiliations || "",
			        	jobsCount: snapshot.val().openJobsCount || 0
			        }
			        self.finalizeBooking( self.userDetail )
	              }
	        )
	        
	        
	      } else {
	        // No user is signed in.
	        console.log( "No User" )
	      }
	    });
	}

	proceedBooking(){
		 let successBooking = this.modalCtrl.create(BookingSuccess);
   			 successBooking.present();
	}

	postJob(dataToSend: any) {
		var self = this;
		//allow only 5 openned jobs
		if( self.userDetail.jobsCount < 10 ){
			self.jobsByUser = self.af.list('/jobsByUser/' + self.userDetail.uid)
	    	
	    	self
	    		.jobsByUser
	    		.push( dataToSend )
	    		.then( data => {
	    			console.log( data.key )
	    			self.jobsBySubCat = self.af.object('/jobs' + "/" + dataToSend.subcatId + "/" + data.key);
	    			self.jobsBySubCat.set( dataToSend )

	    			//update his jobscount
	    			self.af.object( `/users/` + self.userDetail.uid ).update( {"openJobsCount": (self.userDetail.jobsCount + 1)} )
	    		} )
	    	self.proceedBooking()
	    }else{
	    	self.presentAlert( 'Sorry', "We can only allow 10 Open jobs for now", "Ok" )
            // });
	    }
	}

	logError( err: any ){
		console.log( err )
	}

	presentAlert( title: string, subTitle: string, button: string ) {
	    var self = this;
	    let alert = this.alertCtrl.create({
	      title: title,
	      subTitle: subTitle,
	      buttons: [{
	          text: button,
	          handler: data =>{
	            self.closePage()
	          }
	      }]
	    });
	    alert.present();
	}

	closePage(){
  		this.viewCtrl.dismiss();
  	}

}