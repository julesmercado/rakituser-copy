import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController,LoadingController , App} from 'ionic-angular';
import { Page1 } from '../../../page1/page1';
/*
  Generated class for the BookingSuccess page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'booking-success',
  templateUrl: 'booking-success.html'
})
export class BookingSuccess{

  constructor(public navCtrl: NavController,
   private navParams: NavParams,
    private modalCtrl: ModalController, 
    private viewCtrl: ViewController, 
    private loadingCtrl: LoadingController,
    public app: App) {}


 presentLoading() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Please wait...',
      showBackdrop: true,
      dismissOnPageChange: false,
      duration: 100
                    });
    loading.present();
    console.log("spinner");
  }


  done(){

    this.viewCtrl.dismiss();
    this.presentLoading();
    this.app.getRootNav().setRoot(Page1);
	  }

}
