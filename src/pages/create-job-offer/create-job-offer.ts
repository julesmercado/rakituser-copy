import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ModalController  } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookingOfferModal } from './booking-offer-modal/booking-offer-modal';
import { MapDisplay } from '../map-display/map-display';
//import { validateBudget } from '../.././validators/budget-validator';

import * as firebase from 'firebase/app';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';

@Component({
  selector: 'page-create-job-offer',
  templateUrl: 'create-job-offer.html'
})



export class CreateJobOffer{
 	bookTalentForm  : FormGroup;
  output          : any;
  formData        : any;
  subCat          : any;
  _isSubmitted    : boolean=false;
  mode            : number;
  holderVal       : string;
  perks           : string;
  holder          : FirebaseObjectObservable<any>;
  now             : any;

  @ViewChild('map') mapElement;
  map: any;
  marker: any;
  markers: any = [];
  infowindow: any;
  geocoder: any;
  subcatName: string;
  constructor(
  		public navCtrl  		: NavController, 
  		public navParams 		: NavParams, 
  		public modalCtrl 		: ModalController,
  		builder 				    : FormBuilder,
      private af          : AngularFireDatabase
  	) {

  	this.bookTalentForm = builder.group({
        'booking_talent'              : [null, Validators.required],
        'booking_location'            : [null, Validators.required],
        'booking_date'                : [null, Validators.required],
        'booking_time'                : [null, Validators.required],
        'booking_budget_start'        : [null, Validators.required],
        'booking_budget_end'          : [null, Validators.required],/*, 
                                                validateBudget(
                                                  this.bookTalentForm.controls.booking_budget_start, 
                                                  this.bookTalentForm.controls.booking_budget_end)*/
        'booking_description'         : [null, Validators.required],
        'booking_latLng'              : [null, Validators.required],
        'perks'                       : [null]
    })
  	
    this.getSubCat();
    this.bookTalentForm.valueChanges.subscribe(data => {
      this.formData = data;
      this.checkBudgetEndIfHigher(  )
    })
    this.now = new Date();
  }

  ngOnInit(){
    var self = this;
        self.getSubCat();
        let latLng = new google.maps.LatLng(14.5995, 120.9842);
        let mapOptions = {
          center: latLng,
          zoom: 18,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        self.geocoder = new google.maps.Geocoder;
        self.infowindow = new google.maps.InfoWindow;

        self.map = new google.maps.Map(self.mapElement.nativeElement, mapOptions)
        self.marker = new google.maps.Marker({
            position: latLng,
            map: this.map,
            draggable: true
          });
        google.maps.event.addListener(self.marker, 'dragend', function(event) {
          // updateMarkerStatus('Drag ended');
          // geocodePosition(marker.getPosition());
          self.geocodePosition(self.marker.getPosition());
          self.bookTalentForm.controls['booking_latLng'].setValue( self.marker.getPosition().lat() + "-" + self.marker.getPosition().lng() )

          
        });

        self.getHolder()
  };

  getHolder(){
    var self = this;
    console.log( "create" )
        self.holder = self.af.object(`/config`);
        self.holder.subscribe(
                val => {
                  console.log( val )
                  self.holderVal = val.description;
                  self.perks = val.perks;
                }
          )
  }

  getSubCat(){
    var Music = [{
        display: 'Singer',
        selected: true
    },{
        display: 'Dancer',
        selected: true
    },{
        display: 'Live Band',
        selected: false
    },{
        display: 'DJ',
        selected: false
    },{
        display: 'Pianist',
        selected: false
    },{
        display: 'Violinist',
        selected: false
    },{
        display: 'Guitarist',
        selected: false
    },{
        display: 'Drummer',
        selected: false
    }];

    var Photography = [{
        display: 'Photographer',
        selected: true
    },{
        display: 'Videographer',
        selected: false
    }];

    var Graphic = [{
        display: 'Traditional Artist',
        selected: true
    },{
        display: 'Digital Artist',
        selected: false
    }];

    var Food = [{
        display: 'Cook',
        selected: true
    },{
        display: 'Pastry Chef',
        selected: false
    },{
        display: 'Sweets Expert',
        selected: false
    },{
        display: 'Baker',
        selected: false
    }];

    switch( this.navParams.data ){

        case 0:
          this.subCat = Music;
          this.subcatName = "Music";
          break;
        case 1:
          this.subCat = Photography;
          this.subcatName = "Photography";
          break;
        case 2:
          this.subCat = Graphic;
          this.subcatName = "Graphic Design";
          break;
        case 3:
          this.subCat = Food;
          this.subcatName = "Food";
          break;
        default:
          break;
    }
  }

  checkBudgetEndIfHigher(  ): void{
    let start = this.bookTalentForm.get('booking_budget_start');
    let end   = this.bookTalentForm.get('booking_budget_end');
    if( start.status == 'VALID' && end.status == 'VALID' ){
        //let result = end.value > start.value;
        //console.log( this.bookTalentForm )
        //if( !result ) end.status = 'INVALID';
    }
    
  }

  onChange( talent : any ){
    console.log( talent )

  }


  geocodePosition(pos) {
    var self = this;
    self.geocoder.geocode({
      latLng: pos
    }, function(responses) {
      if (responses && responses.length > 0) {
        self.marker.formatted_address = responses[0].formatted_address;
        self.bookTalentForm.controls['booking_location'].setValue( self.marker.formatted_address )
      } else {
        self.marker.formatted_address = 'Cannot determine address at this location.';
      }
      self.infowindow.setContent(self.marker.formatted_address + "<br>coordinates: " + self.marker.getPosition().toUrlValue(6));
      self.infowindow.open(self.map, self.marker);
    });
  }




  viewMap(){
  	let showMap = this.modalCtrl.create(MapDisplay);
  		showMap.present();
  }

  submitForm(form: any , value: any): void{
    this._isSubmitted = true;
    if( form && this._isSubmitted ){
      this.validateBooking( value )
    }
  }

  validateBooking( details ){

    this.navCtrl.push(BookingOfferModal, { bookDetails: details, cat: this.subcatName });
  }

   dualValue2={'lower': '250', 'upper': '1500'};
}
