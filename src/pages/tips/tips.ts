import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController,LoadingController , App} from 'ionic-angular';
import { Page1 } from '../page1/page1';

import * as firebase from 'firebase/app';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

/*
  Generated class for the Tips page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'tips',
  templateUrl: 'tips.html'
})
export class Tips{
  tips: FirebaseListObservable<any>;
  tipsList: any = [];
  buttonIs: boolean = true;
  constructor(public navCtrl: NavController,
   private navParams: NavParams,
    private modalCtrl: ModalController, 
    private viewCtrl: ViewController, 
    private loadingCtrl: LoadingController,
    private app: App,
    private af: AngularFireDatabase) {}

  ngOnInit(){
    this.getTips()
  }

  getTips(){
    console.log( "tips" )
    var self = this;
        self.tips = self.af.list(`/tips/rakit-user`);
        self.tips.subscribe(
                val => {
                  console.log( val )
                  self.tipsList = val;
                  self.buttonIs = false;
                  console.log( self.buttonIs )
                }
          )
  }

  presentLoading() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Please wait...',
      showBackdrop: true,
      dismissOnPageChange: false,
      duration: 100
                    });
    loading.present();
    console.log("spinner");
  }


  done(){
    this.presentLoading();
    this.app.getRootNav().setRoot(Page1);
	  }

}
