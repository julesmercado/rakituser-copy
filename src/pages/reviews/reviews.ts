import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ModalController, LoadingController } from 'ionic-angular';

import * as firebase from 'firebase/app';
/*
  Generated class for the Notifications page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-reviews',
  templateUrl: 'reviews.html'
})
export class Reviews{
  reviews: any = [];
  starsCount: number;
  readOnlyThis: boolean = false;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private viewCtrl: ViewController ) {
    this.readOnlyThis = true;
  }

  ngOnInit(){
    var self = this;
    //self.user = UserService;
    // if user is logged in to facebook
    self.getReviews()
  }

  clickCount( count: number ){
    console.log( count )
    this.readOnlyThis = true;
  }
  closePage(){
      this.viewCtrl.dismiss();
  }

  getReviews( ){
    var self = this;
      self.reviews = [];
      firebase.auth().onAuthStateChanged(function(userAf) {
        var rootRef = firebase.database().ref();
        var userRef = rootRef.child("reviews").child(userAf.uid);
        var query   = userRef
                          .child("asClient");
        query.on('child_added', 
          snap => {
            var obj = {
              "star": snap.val().star, 
              "review": snap.val().review, 
              "name": snap.val().name, 
              "photoURL": snap.val().photoURL,
              "key": snap.val().key
            }
            self.reviews.push( obj )
        })
        // userRef.once('value', function( snap ){
        //   if( snap.val() != null ){
        //     profile.meanStarCountClient = snap.val().meanStarCountClient || 0;
        //     console.log( snap.val() )
        //   }
        // })
      })
  }
}
