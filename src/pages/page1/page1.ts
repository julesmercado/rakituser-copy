import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { CreateJobOffer } from '../create-job-offer/create-job-offer';
import { Notifications } from '../notifications/notifications';
import { UserService } from '../../providers/user-service';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import * as firebase from 'firebase/app';

import { Profile } from '../profile/profile';
@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html',
  providers: [ UserService ]
})
export class Page1 {
  userInfo: any;
  user: UserService = {};
  items: FirebaseObjectObservable<any>;
  constructor(
    public navCtrl: NavController, 
    private loadingCtrl: LoadingController, 
    private alertCtrl: AlertController,
    private af: AngularFireDatabase,
    public modalCtrl: ModalController,) {
    
    
  }

  ionViewDidLoad() {
    var self = this;
    self.getUser()
  }
  
//spinner loading
  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Please wait...',
      showBackdrop: true,
      dismissOnPageChange: false,
      duration: 1000
                    });
    loading.present();
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Oops!',
      subTitle: 'Let\'s complete your profile to post jobs',
      buttons: [{
          text: "Ok",
          handler: data =>{
            let openNavPage = this.modalCtrl.create(Profile);
            openNavPage.present();
          }
      }]
    });
    alert.present();
  }


	goOrder(mode:number) {
      var self = this;
      self.presentLoadingDefault();
      if( self.userInfo.phone == "" || self.userInfo.affiliations == "" 
          || self.userInfo.phone == null || self.userInfo.affiliations == null ){
        self.presentAlert()
      }else{
        self.navCtrl.push(CreateJobOffer, mode); 
      }
      
      //this.presentAlert()
  }

  	goNotifs(){
        this.presentLoadingDefault();
  	    this.navCtrl.push(Notifications); 
  	}

  getUser(){
    var self = this;
    firebase.auth().onAuthStateChanged(function(userAf) {
      if (userAf) {
        self.userInfo = {
          name: userAf.displayName,
          email: userAf.email || "",
          photo: userAf.photoURL,
          uid: userAf.uid
        }
        self.items = self.af.object('/users/' + userAf.uid);
        self.items.subscribe(
              val => {
                self.userInfo.phone = val.phone || "";
                self.userInfo.affiliations = val.affiliations || "";
              }
        )
      } else {
        // No user is signed in.
      }
    });
  }
}
