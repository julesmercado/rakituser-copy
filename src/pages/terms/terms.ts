import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';


/*
  Generated class for the CustomerSupport page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-terms',
  templateUrl: 'terms.html'
})
export class Terms {
  
  
  constructor( public viewCtrl: ViewController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad Privacy Page');
  }

   dismiss(){
  		this.viewCtrl.dismiss();
  }

  

}
