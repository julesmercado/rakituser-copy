import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';


/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-full-image',
  templateUrl: 'full-image.html'
})
export class FullImage{
  image: string;
  constructor( private viewCtrl: ViewController, private navParams: NavParams ) {
    this.image = navParams.get('img');
    console.log( this.image )
  }

  ionViewDidLoad() {
      var self = this;
  }

  closePage(){
  		this.viewCtrl.dismiss();
  }

}
