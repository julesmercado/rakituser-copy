import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicPageModule, IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { HttpModule, JsonpModule } from '@angular/http';
import { Broadcaster } from '@ionic-native/broadcaster';
import { NativeStorage } from '@ionic-native/native-storage';
import { MomentModule } from 'angular2-moment';
import { RatingModule } from "ngx-rating";


import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Login } from '../pages/login/login';
import { CreateJobOffer } from '../pages/create-job-offer/create-job-offer';
import { BookingOfferModal } from '../pages/create-job-offer/booking-offer-modal/booking-offer-modal';
import { BookingSuccess} from '../pages/create-job-offer/booking-offer-modal/booking-success/booking-success';
import { Notifications } from '../pages/notifications/notifications';
import { NotificationsBids } from '../pages/notifications/notifications-bids/notifications-bids';
import { Profile } from '../pages/profile/profile';
import { MyJobOffer } from '../pages/my-job-offer/my-job-offer';
import { CustomerSupport } from '../pages/customer-support/customer-support';
import { MapDisplay } from '../pages/map-display/map-display';
import { JobDetails } from '../pages/job-details/job-details';
import { FullImage } from '../pages/full-image/full-image';
import { Tips } from '../pages/tips/tips';
import { Reviews } from '../pages/reviews/reviews';
import { Privacy } from '../pages/privacy/privacy';
import { Terms } from '../pages/terms/terms';

import { ReactiveFormsModule } from '@angular/forms';

//Validators
//import { validateBudget } from '../validators/budget-validator';


//import { FbProvider } from '../providers/fb-provider';

export const firebaseConfig = {
  apiKey: "[your_api_key]",
  authDomain: "[your_fbase_app_name].firebaseapp.com",
  databaseURL: "[your_fbase_app_name].firebaseio.com",
  storageBucket: "[your_fbase_app_name].appspot.com",
  messagingSenderId: "[your_sender_id]"
};

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Login,
    CreateJobOffer,
    BookingOfferModal,
    BookingSuccess,
    Notifications,
    NotificationsBids,
    Profile,
    MyJobOffer,
    CustomerSupport,
    MapDisplay,
    JobDetails,
    FullImage,
    Tips,
    Reviews,
    Privacy,
    Terms
  ],
  imports: [
    IonicModule.forRoot(MyApp, {}),
    BrowserModule,
    HttpModule,
    MomentModule,
    RatingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
 ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Login,
    CreateJobOffer,
    BookingOfferModal,
    BookingSuccess,
    Notifications,
    NotificationsBids,
    Profile,
    MyJobOffer,
    CustomerSupport,
    MapDisplay,
    JobDetails,
    FullImage,
    Tips,
    Reviews,
    Privacy,
    Terms
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, NativeStorage, Broadcaster, StatusBar]
})

export class AppModule {



}
