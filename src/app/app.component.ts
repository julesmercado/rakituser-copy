import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController, LoadingController, Events} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { Page1 } from '../pages/page1/page1';
import { Login} from '../pages/login/login';
import { Profile } from '../pages/profile/profile';
import { Reviews} from '../pages/reviews/reviews';
import { MyJobOffer } from '../pages/my-job-offer/my-job-offer';
import { BookingSuccess} from '../pages/create-job-offer/booking-offer-modal/booking-success/booking-success';
import { CustomerSupport } from '../pages/customer-support/customer-support';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { UserService } from '../providers/user-service';

import * as firebase from 'firebase/app';

@Component({
  templateUrl: 'app.html',
  providers: [ Facebook, StatusBar, UserService ]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  user: any;
  rootPage: any = Login;
  pages: Array<{title: string, component: any, icon: any}>;
  userInfo: any;

  constructor(public platform: Platform,  
              public modalCtrl: ModalController,
              public events: Events) {
    const firebaseConfig = {
        apiKey: "",
        authDomain: "",
        databaseURL: "",
        storageBucket: "",
        messagingSenderId: ""
    };
    //this.user = this.userService;
    this.initializeApp();
    //firebase.initializeApp(firebaseConfig);
    // used for an example of ngFor and navigation
    this.pages = [
      //{ title: 'Find Artists', component: Page1, icon: 'search' } ,
      { title: 'Profile', component: Profile, icon: 'contact'},
      { title: 'My Job Offers', component: MyJobOffer, icon: 'briefcase'},
      //{ title: 'Invite Friends', component: Login, icon: 'phone-portrait'},
      //{ title: 'Rate Us', component: Login, icon: 'heart'},
      { title: 'My Reviews', component: Reviews, icon: 'star'},
      { title: 'Support', component: CustomerSupport, icon: 'help-circle'},   
    ];
    this.events.subscribe('user:ready', (res) => {
          this.userInfo = res;
          this.editProf( this.userInfo )
    });
  }

  editProf( prof: any ){
    var self = this;
    firebase.auth().onAuthStateChanged(function(userAf) {
        if (userAf) {
          firebase.database().ref('users').child(userAf.uid).update(prof)
        } else {
          // No user is signed in.
          console.log( "No User" )
        }
      });
  }

  initializeApp() {

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      //StatusBar.backgroundColorByHexString("#222"); 
      //StatusBar.styleLightContent();
      //Keyboard.hideKeyboardAccessoryBar(false);
    });


    
  }
  

  openPage(page) {
    console.log(page);

    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if(page.component != Page1 && page.component != Login){
         let openNavPage = this.modalCtrl.create(page.component);
          openNavPage.present();
    }
    
    else{
          this.nav.setRoot(page.component);
    }


  }



  
}

