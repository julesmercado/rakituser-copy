import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the Services provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Services {
	userInfo: any;
  headers: any;
  options: any;
	//private url = 'https://rakit-server.herokuapp.com';
	private url = 'http://rakit-test-one.herokuapp.com';

  constructor(public http: Http) {
  	  this.headers = new Headers({ 'Content-Type': 'application/json' });
      this.options = new RequestOptions({ headers: this.headers });
	}

  	// getUserInfo(): Observable<Product[]> {
   //      return this.http.get(this.url)
   //          .map((response: Response) => <Product[]>response.json())
   //          .catch(this.handleError);
   //  }

    // getSubCat(): Observable<Response> {/**/

    //     return this.http.get('http://date.jsontest.com')
    //         .map(data => {
    //                 data.json();
    //                 // the console.log(...) line prevents your code from working 
    //                 // either remove it or add the line below (return ...)
    //                 console.log("I CAN SEE DATA HERE: ", data.json());
    //                 return data.json();
    //         })
    //         .catch();
    // }

    private extractData(res: Response) {
      let body = res.json();
      return body.data || { };
    }
    private handleError (error: Response | any) {
      // In a real world app, you might use a remote logging infrastructure
      let errMsg: string;
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      } else {
        errMsg = error.message ? error.message : error.toString();
      }
      console.error(errMsg);
      return Observable.throw(errMsg);
    }

   //  setUserInfo( data: any ){
   //  	this.userInfo = data;
   //  }

   //  getUserInfoData(  ){
   //  	return this.userInfo
   //  }

    createJob(job: any) {                
        let body = JSON.stringify(job);            
        

        return this.http.post(this.url + '/api/add/job/offer', job, this.options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    

   //  private extractData(res: Response) {
   //      let body = res.json();
   //      return body.data || {};
   //  }


}
